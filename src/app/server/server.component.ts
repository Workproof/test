import {Component} from '@angular/core';

@Component({
	selector:'[app-server]',
	templateUrl:'./server.component.html',
	styles:[`
        .online{
          color:blue;
        }
`]
})

export class ServerComponent{
	serverId:number=10;
	serverEstatus:string ='offline';
     

constructor (){
	this.serverEstatus=Math.random() > 0.5 ? 'Online' :'Offline';
	console.log(this.serverEstatus);
}


	getServerId(){
	return this.serverId;
	}
	getServerStatus(){
	return this.serverEstatus;
	}

getColor(){
	return this.serverEstatus==='Online'? 'red': 'gray';
}

}