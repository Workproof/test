import { Component, OnInit } from '@angular/core';


@Component({
selector:'[app-servers]',
templateUrl:'./servers.component.html',
})
export class ServersComponent implements OnInit {
 allowNewServer=false;
 serverCreationStatus='No server was created';
 severValueEvent='test';
 serverBol=false;
 servers=['TestServer','TestServer2'];

  constructor() { 
   setTimeout(()=>{
     this.allowNewServer=true;
   },2000);
  }

  ngOnInit() {
  }

  onCreateServer(){
  this.serverBol=true;
  this.servers.push(this.severValueEvent);
  this.serverCreationStatus='Server was created. Name is:' + this.severValueEvent;
  }

  onUpdateServerName(event:Event){
  this.severValueEvent=(<HTMLInputElement>event.target).value;
  console.log(this.severValueEvent);
  }

}
